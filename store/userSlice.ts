import { createSlice, PayloadAction } from '@reduxjs/toolkit'

export interface IUserInfo {
    user_id: string;
    first_name: string;
    last_name: string;
    email: string;
    user_type: string
    primary_contact_number: string
}

export interface UserState {
  access_token: string,
  user: IUserInfo
}

const initialState: UserState = {
  access_token: "null",
  user: {
    user_id: "guest-user-id",
    first_name: "guest",
    last_name: "guest",
    email: "guest.guest@gmail.com",
    user_type: "ANONYMOUS",
    primary_contact_number: "--"
  }
}

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUserState: (state, action: PayloadAction<UserState>) => {
      state.user = { ...state.user, ...action.payload.user};
      state.access_token = action.payload.access_token;
    },
    setUserInfo: (state, action: PayloadAction<IUserInfo>) => {
      state.user = action.payload; 
    },
    updateUserAccessToken: (state, action: PayloadAction<string>) => {
      state.access_token = action.payload;
    },
    updateUserInfo: (state, action: PayloadAction<IUserInfo>) => {
      state.user = { ...state.user, ...action.payload }
    },
    resetUserInfo: state => {
      state = initialState
    }
  }
})

export const { setUserState, setUserInfo, updateUserAccessToken, updateUserInfo, resetUserInfo } = userSlice.actions
export default userSlice.reducer