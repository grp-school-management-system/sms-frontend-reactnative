import { Stack, useNavigation } from 'expo-router';
import { Button, Text, Image, StyleSheet } from 'react-native';
import { View } from 'react-native';
import { useEffect } from 'react';

export default function Admin() {
  const navigation = useNavigation();

  useEffect(() => {
    navigation.setOptions({ headerShown: false })
  }, [navigation]);

  return (
    <View>
      <Text>Admin</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    width: 50,
    height: 50,
  },
});
