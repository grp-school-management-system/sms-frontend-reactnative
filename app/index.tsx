import React, { useCallback, useEffect, useState } from 'react';
import { Button, Text, View, Pressable, SafeAreaView, ScrollView } from 'react-native';
import Entypo from '@expo/vector-icons/Entypo';
import * as SplashScreen from 'expo-splash-screen';
import * as Font from 'expo-font';
import { useAppDispatch, useAppSelector } from '../store/hooks';
import { store } from '../store/store';
import { increment, decrement, incrementByAmount } from '../store/counterSlice';
import { Link } from "expo-router";
import { MainHeader } from '../components/header/Header';
import { Divider, useTheme } from 'react-native-paper';
import { SlotOne } from '../components/home/SlotOne';
import { SlotTypeTwo } from '../components/home/SlotTypeTwo';
import { SlotFive } from '../components/home/SlotFive';
import { homeSlotFourData, homeSlotThreeData, homeSlotTwoData } from '../utils/data/home_slot_two';
import { SlotTypeThree } from '../components/home/SlotTypeThree';
SplashScreen.preventAutoHideAsync();

export default function AppIndex() {
  const [appIsReady, setAppIsReady] = useState(false);
  const count = useAppSelector(state => state.counter.value);
  const dispatch = useAppDispatch();
  const theme = useTheme()


  useEffect(() => {
    async function prepare() {
      try {
        await Font.loadAsync(Entypo.font);
        await new Promise(resolve => setTimeout(resolve, 2000));
      } catch (e) {
        console.warn(e);
      } finally {
        console.log("App is ready");
        setAppIsReady(true);
      }
    }

    prepare();
  }, []);

  const onLayoutRootView = useCallback(async () => {
    if (appIsReady) {
      console.log("hide splash screen");
      await SplashScreen.hideAsync();
    }
  }, [appIsReady]);

  if (!appIsReady) {
    return null;
  }

  return (
    <View
      style={{ 
        height: '100%',
        width: '100%',
        backgroundColor: '#abbaca'
      }}
      onLayout={onLayoutRootView}
      >
        <Divider/>
        <SafeAreaView style={{flex: 1}}>
          <ScrollView
            contentContainerStyle={{
              height: 800,
              backgroundColor: '#a2a212'
            }}>
            <View style={{
              height: 100,
              width: '100%',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}>
                <SlotOne />
                <SlotTypeTwo data={homeSlotTwoData} />
                <SlotTypeTwo data={homeSlotThreeData} />
                <SlotTypeThree data={homeSlotFourData} />
                <SlotFive />
            </View>
          </ScrollView>
        </SafeAreaView>        
    </View>
  );
}
