import { Stack } from 'expo-router';
import { colorPallet } from '../constants/Colors';
import React from 'react';
import { Provider } from 'react-redux';
import { store } from '../store/store';
import { MainHeader } from '../components/header/Header';
import { MD3LightTheme as DefaultTheme, PaperProvider } from 'react-native-paper';
import { MainFooter } from '../components/header/Footer';
import { SafeAreaView, ScrollView, ScrollViewBase } from 'react-native';
import { View, Text } from 'react-native';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#87CEEB'
  },
};
export default function RootLayout() {

  return (
    <Provider store={store}>
      <PaperProvider theme={theme}>
        <RootLayoutNav />
      </PaperProvider>
    </Provider>
  );
}

const RootLayoutNav = (props: any) => {
  return (
    
        <Stack screenOptions={{
          header: props => <MainHeader/>
        }}>
          <Stack.Screen name="index"></Stack.Screen>
        
        </Stack>
        
    
  )
}