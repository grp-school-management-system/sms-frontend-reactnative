import { IHomeSlotProps } from "../../components/home/interfaces";

export const homeSlotTwoData: IHomeSlotProps = {
  title: "Implement progressive instructional practices",
  description: "Support your learning community with both traditional and progressive instructional practices.",
  paneData: {
    left: [
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Standard Based Gradebook"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Supports Any Standards (including Custom)"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Blended Learning"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Personalized Learning Schedules"
      }
    ],
    right: [
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Standard Tracking"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Supports Any Rubric (including Custom)"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Differentiated Assignment"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Google Classroom Integration"
      }
    ]
  }
};

export const homeSlotThreeData: IHomeSlotProps = {
  title: "Increase adotption across your learning community",
  description: "Finally, an SIS you team, your students, and their parents will rally around and use.",
  paneData: {
    left: [
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Easy to use"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Increased Student, Parents, and Family engagement"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Modern tools promote technology-oriented culture and ditital literacy"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Well-received tools that enablle progressive practices"
      }
    ],
    right: [
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Customizable to district and school"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Intuitive interface"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Widespread usage by staff compared to other platforms"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Massive time savings for entire staff"
      }
    ]
  }
}


export const homeSlotFourData: IHomeSlotProps = {
  title: "One platform. Hundreds of features. Limitless Insight.",
  description: "we aims to deliver all the features your district needs with none of the headaches you expect.",
  paneData: {
    left: [
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Calandars"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Calandars"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Google Classroom Integration"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Biographical"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Students and Parent Portal"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Roster"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Schedule tracking"
      }
    ],
    right: [
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Curriculum mapping"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Emergency and system wide communication"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Standards based gradebook"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "In app customer support"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Report Card Builder"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "Class, School and District wide view"
      },
      {
        iconSrc: "../assets/images/react-logo.png",
        text: "State reporting"
      }
    ]
  }
}