/**
 * Below are the colors that are used in the app. The colors are defined in the light and dark mode.
 * There are many other ways to style your app. For example, [Nativewind](https://www.nativewind.dev/), [Tamagui](https://tamagui.dev/), [unistyles](https://reactnativeunistyles.vercel.app), etc.
 */

const tintColorLight = '#0a7ea4';
const tintColorDark = '#fff';

export const Colors = {
  light: {
    text: '#11181C',
    background: '#fff',
    tint: tintColorLight,
    icon: '#687076',
    tabIconDefault: '#687076',
    tabIconSelected: tintColorLight,
  },
  dark: {
    text: '#ECEDEE',
    background: '#151718',
    tint: tintColorDark,
    icon: '#9BA1A6',
    tabIconDefault: '#9BA1A6',
    tabIconSelected: tintColorDark,
  },
};

export const colorPallet = {
  caribbeanCurrent: '#006d77',
  tiffanyBlue: '#83c5be',
  verdigris: '#38a3a5',
  emerald: '#57cc99',
  teaGreen: '#c7f9cc',
  berkeleyBlue: '#1d3557',
  cambridgeBlue: '#84a98c',
  aliceBlue: '#edf6f9',
  airSuperiorityBlue: '#669bbc',
  celadon: '#BBD8B3',
  moonStone: '#7cabba',
}