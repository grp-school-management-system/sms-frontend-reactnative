import { View, Image, Text, StyleSheet } from "react-native";
import { IHomePaneElementProps, IHomeSlotProps } from "./interfaces";


const SlotPaneElementComponent = (props: {element: IHomePaneElementProps}) => {
  const { iconSrc, text } = props.element;
  return (<View
      style={[styles.row_flex, styles.slot_pane_element]}>
    <Image 
      style={[styles.slot_pane_element_icon]}
      source={{ uri: iconSrc }}/>
    <Text style={[styles.slot_common_text]}>{text}</Text>
  </View>);
}

const SlotPaneComponent = (props: {dataList: IHomePaneElementProps[]}) => {
  const { dataList } = props;
  return <View
  style={[styles.column_flex, styles.slot_pane]}>
  {
    dataList.map((element: IHomePaneElementProps, index: number) =>
    (<SlotPaneElementComponent key={`unique-key-${index}`} element={element}/>))
  }
</View>
} 

export const SlotTypeTwo = (props: { data: IHomeSlotProps }) => {
  const {title, description, paneData} = props.data;
  return <View style={[styles.column_flex, styles.slot_container]}>
    <View style={[styles.column_flex]}>
      <View> 
        <Image></Image>
      </View>
      <View style={[styles.column_flex]}>
        <Text style={[styles.slot_title]}>{title}</Text>
        <Text style={[styles.slot_description]}>{description}</Text>
      </View>
    </View>
  <View
    style={[styles.row_flex, styles.slot_pane]}>
      <SlotPaneComponent dataList={paneData.left}/>
      <SlotPaneComponent dataList={paneData.right}/>
  </View>
</View>
}

const styles = StyleSheet.create({
  column_flex: {
    display: 'flex',
    flexDirection: 'column',
  },
  row_flex: {
    display: 'flex',
    flexDirection: 'row',
  },
  slot_container: {
    height: 600,
    marginTop: 10,
    padding: 20,
    width: '100%',
    backgroundColor: '#cbcfa1',
    justifyContent: 'flex-end',
    alignContent: 'flex-end',
    alignItems: 'center'
  },
  slot_common_text: {
    fontSize: 16,
    fontWeight: 500,
    color: '#834aa0'
  },
  slot_title: {
    fontSize: 30,
    fontWeight: 400,
    margin: 10,
    color: '#0a0aaa'
  },
  slot_description: {
    fontSize: 18,
    fontWeight: 300,
    margin: 10,
    color: '#0a0aaa'
  },
  slot_pane: {
    padding: 10,
    margin: 10
  },
  slot_pane_element: {
    margin: 10,
    padding: 10,
    alignContent: 'center',
    alignItems: 'center'
  },
  slot_pane_element_icon: {
    height: 50,
    width: 50,
    marginRight: 10
  },
});