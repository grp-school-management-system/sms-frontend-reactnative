import { View, Text, Button, Image, StyleSheet, Pressable } from 'react-native';

const texts = {
  home: {
    title_one: "Scool Management System",
    description: "Simplify And Succeed",
    additional_info: "Easy-to-use software to help your school save time, improve enrollment and fultill its mission",
    btn_title: "Watch A Demo"
  }
}


export const SlotOne = (props: any) => {
  return <View  
    style={{
      display: 'flex',
      flexDirection: 'row',
      alignContent: 'center',
      justifyContent: 'space-around',
      paddingTop: 50,
      paddingLeft: 40, 
      marginTop: 40,
      height: 500
      // backgroundColor: '#bbbbbb'
    }}
  >
    <View
      id='home-desciption-section'
      style={{
        display: 'flex',
        flexDirection: 'column',
        paddingLeft: 30,
        marginTop: 60,
        marginLeft: 60,
        width: '35%'

      }}
    >
      <Text style={styles.home_slot_one_title}>{texts.home.title_one}</Text>
      <Text style={styles.home_slot_one_description}>{texts.home.description}</Text>
      <Text style={styles.home_slot_one_additional_info}>{texts.home.additional_info}</Text>
      <Pressable style={styles.home_slot_one_btn}>
        <Text style={{fontWeight: 'bold', color: '#0a0aaa' }}>{texts.home.btn_title}</Text></Pressable>
    </View>
    <View
      id='demo-video-section'
      style={{
        width: '40%',
        margin: 20
      }}
    >
      <View
        style={{
          height: 300,
          width: 450,
          shadowColor: 'black',
          shadowOffset: { width: 3, height: 3 },
          shadowOpacity: 1,
          shadowRadius: 2,
          elevation: 2
        }}
      >
        <Image style={{
          height: '100%',
          width: '100%'
        }} source={{ uri: "../assets/images/Home-video-thumbnail-1.png" }} />
      </View>
    </View>
  </View>
}

const styles = StyleSheet.create({
  home_slot_one_title: {
    fontSize: 20,
    padding: 10,
    fontWeight: 600,
    color: '#0a0aaa'
  },
  home_slot_one_description: {
    fontSize: 40,
    padding: 7,
    fontWeight: 600,
    color: '#834aa0'
  },
  home_slot_one_additional_info: {
    fontSize: 20,
    padding: 10,
    fontWeight: 400,
    color: '#0a0aaa'
  },
  home_slot_one_btn: {
    paddingLeft: 10,
    marginLeft: 10,
    fontWeight: 800,
    borderRadius: 10,
    textAlign: 'center',
    justifyContent: 'center',
    backgroundColor: '#cbcfa1',
    width: 130,
    height: 40
  }
});