export interface IHomePaneElementProps {
  iconSrc: string;
  text: string;
}

export interface IHomePaneProps {
  left: IHomePaneElementProps[];
  right: IHomePaneElementProps[];
}

export interface IHomeSlotProps {
  title: string;
  description: string;
  paneData: IHomePaneProps;
}