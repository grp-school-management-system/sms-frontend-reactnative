import { View, Image, Text } from "react-native"
import { useAppSelector } from "../../store/hooks"

export const HeaderProfile = () => {
  const userDetail = useAppSelector(state => state.user.user);

  return <View style={{ 
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }}>
    <Image style={{ height: 50, width: 50, margin: 10 }}source={{uri: '../../assets/images/profile-icon.png'}}></Image>
    <Text>{ userDetail.first_name + " " + userDetail.last_name }</Text>
  </View>
}