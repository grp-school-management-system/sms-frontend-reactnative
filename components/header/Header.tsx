import { colorPallet } from "../../constants/Colors";
import { LoginSignupComponent } from "../LoginSignupComponent"
import { Image, Text, StyleSheet, View } from 'react-native';
import { useTheme } from 'react-native-paper';
import { WhyLearnAscendMenu } from "../menus/WhyLearnAscend";
import { ResourcesMenu } from "../menus/ResourcesMenu";
import { ProductsMenu } from "../menus/ProductsMenu";
import { HeaderProfile } from "./HeaderProfile";
import { useAppSelector } from "../../store/hooks";
import { GalleryMenu } from "../menus/GalleryMenu";
import { GetQuoteMenu } from "../menus/GetQuoteMenu";


interface IHeaderIcon {

}

const HeaderIcon = (IHeaderIcon: any) => {
  return (
    <View>
      <Image style={styles.image} source={{ uri: "../assets/images/student-connect-2.png" }} />
    </View>
  )
}

interface IHeaderMenu {

}

const GuestHeaderMenu = (props: IHeaderMenu) => {
  const userState = useAppSelector(state => state.user);
  return (
    <View style={{ 
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      alignContent: 'center'
    }}>
      <WhyLearnAscendMenu />
      <ProductsMenu />
      <ResourcesMenu />
      <GalleryMenu />
      <GetQuoteMenu />
      
      { userState.access_token === "null" && <LoginSignupComponent /> }
    </View>
  )
}

const UserHeaderMenu = (props: IHeaderMenu) => {
  const userState = useAppSelector(state => state.user);
  return (
    <View style={{ 
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      alignContent: 'center'
    }}>
      <WhyLearnAscendMenu />
      <ProductsMenu />
      <ResourcesMenu />
      <GalleryMenu />
      <GetQuoteMenu />
      
      { userState.access_token === "null" && <LoginSignupComponent /> }
    </View>
  )
}

const AddOnTheme = (props: any) => {
  const theme = useTheme()

  return <View style={{
    zIndex: -1000
  }}>
    <View style={{
      backgroundColor: theme.colors.primary,
      position: "absolute",
      zIndex: -100,
      height: 1000,
      width: '100%'
    }}>
    </View>
    <View style={{
      backgroundColor: "#1d6aa2",
      position: "absolute",
      zIndex: -40,
      height: 1700,
      borderRadius: 800,
      width: '100%',
      transform: [{translateY: -850}, {translateX: 0}]
    }}>
    </View>
    <View style={{
      backgroundColor: "#1d6aa2",
      position: "absolute",
      zIndex: -50,
      height: 500,
      width: '100%',
      transform: [{translateY: 0}, {translateX: 0}]
    }}>
    </View>
  </View>
}

export const MainHeader = (props: any) => {
  const theme = useTheme();
  const userState = useAppSelector(state => state.user);

  return <View style={{
    backgroundColor: "#cbcfa1",
    height: 120
  }}>
    {/* <AddOnTheme /> */}
    <View style={styles.header}>
      <HeaderIcon />
      
      { userState.access_token === "null" && <GuestHeaderMenu /> }
      { userState.access_token != "null" && <UserHeaderMenu /> }
      { userState.access_token != "null" && <HeaderProfile /> }
    </View>
  </View>
}

const styles = StyleSheet.create({
  image: {
    width: 180,
    height: 100,
    padding: 10
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    // backgroundColor: colorPallet.cambridgeBlue
  }
});
