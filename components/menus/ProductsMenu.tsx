import * as React from 'react';
import { Pressable, View, Text } from 'react-native';
import { Button, Menu } from 'react-native-paper';

export const ProductsMenu = (props: any) => {
  const [visible, setVisible] = React.useState(true);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);

  return (
    <View style={{ padding: 12, justifyContent: 'flex-end' }}>
      <Menu
        visible={visible}
        onDismiss={closeMenu}
        anchor={<Text
          style={{color: "white", fontWeight: 400, fontSize: 16, textDecorationLine: "underline" }}
          onPress={openMenu}
          >Products
        </Text>}
      >
        <Menu.Item leadingIcon="redo" onPress={() => {}} title="Redo" />
        <Menu.Item leadingIcon="undo" onPress={() => {}} title="Undo" />
        <Menu.Item leadingIcon="content-cut" onPress={() => {}} title="Cut" disabled />
        <Menu.Item leadingIcon="content-copy" onPress={() => {}} title="Copy" disabled />
        <Menu.Item leadingIcon="content-paste" onPress={() => {}} title="Paste" />
      </Menu>
    </View>
  )
};

