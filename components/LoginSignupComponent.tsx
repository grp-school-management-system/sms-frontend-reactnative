import { Button, Modal, Pressable, Alert, View, Text, Image, StyleSheet } from 'react-native';
import { useEffect, useState } from 'react';
import LoginForm from '../components/forms/LoginForm';
import { FormState } from '../components/enums';
import SignupForm from '../components/forms/SignupForm';
import { colorPallet } from '../constants/Colors';

export const LoginSignupComponent = (props: any) => {
  const [loggedIn, setLoggedIn] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [formState, setFormState] = useState(FormState.LOGIN);

  const handleUserLogin = () => {
    setModalVisible(true)
    // setLoggedIn(true);

  }

  const handleUserLogout = () => {
    setLoggedIn(false);
  }

  return <View style={styles.header_signup_login_btn}>
    {!loggedIn
      ? <Text onPress={handleUserLogin} style={styles.textStyle}>Login</Text>
      : <Button
        color={colorPallet.cambridgeBlue}
        title='logout'
        onPress={handleUserLogout}
      />
    }
    <Modal
      animationType="fade"
      style={styles.form_modal}
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        Alert.alert('Modal has been closed.');
        setModalVisible(!modalVisible);
      }}>
      {formState === FormState.LOGIN
        ? <LoginForm setModalVisible={setModalVisible} setFormState={setFormState} />
        : <SignupForm setModalVisible={setModalVisible} setFormState={setFormState} />
      }
    </Modal>
  </View>
}


const styles = StyleSheet.create({
  image: {
    width: 180,
    height: 100,
    padding: 10
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    backgroundColor: colorPallet.cambridgeBlue
  },
  header_signup_login_btn: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    padding: 12
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: colorPallet.cambridgeBlue,
  },

  textStyle: {
    color: 'white',
    fontWeight: 400,
    textDecorationLine: "underline",
    fontSize: 16,
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  form_modal: {
    backgroundColor: "#cbcfa1"
  }
});