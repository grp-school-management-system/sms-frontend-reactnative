import React, { useState } from 'react'
import { Alert, Button, Image, Pressable, SafeAreaView, StyleSheet, Switch, Text, TextInput, View } from 'react-native'
import { LoginService } from '../../services/LoginService';
import { colorPallet } from '../../constants/Colors';
import { FormState } from '../enums';

interface ISignUpFormData {
  firstName: string;
  lastName: string;
  email: string;
  contactNumber: string;
  password: string;
  confirmPassword: string;
}

export default function SignupForm(props: any) {
  const { setModalVisible, formState, setFormState } = props;
  const defaultFormData: ISignUpFormData = {
    firstName: "",
    lastName: "",
    email: "",
    contactNumber: "",
    password: "",
    confirmPassword: "",
  }

  const restService = LoginService.getInstance();
  const [click, setClick] = useState(false);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [signupFormData, setSignupFormData] = useState<ISignUpFormData>(defaultFormData);

  const changeFormState = (state: FormState) => {
    if (formState != state) {
      setFormState(state)
    }
  }

  const handleFormInput = (e: any) => {
    console.log("handle Form Input: ", e)

  }

  const handleSignup = () => {
    const signupPayload: any = {};
    restService.signup(signupPayload)
  }

  return (
    <View style={styles.centeredView}>
      <View style={styles.modalView}>
        <View style={styles.modalHeaderView}>
          <Text style={styles.signupFormHeaderText}><h3>Sign up</h3></Text>
        </View>
        <SafeAreaView style={styles.signupFormContainer}>
          <View style={styles.signupFormInputView}>
            <TextInput
              id="signup-form-first-name"
              style={styles.signupFormInput}
              placeholder='First Name'
              value={signupFormData.firstName}
              onChangeText={handleFormInput}
              autoCorrect={false}
              autoCapitalize='none' />
            <TextInput
              id="signup-form-last-name"
              style={styles.signupFormInput}
              placeholder='Last Name'
              value={signupFormData.lastName}
              onChangeText={handleFormInput}
              autoCorrect={false}
              autoCapitalize='none' />
            <TextInput
              id="signup-form-email"
              style={styles.signupFormInput}
              placeholder='Email'
              value={signupFormData.email}
              onChangeText={handleFormInput}
              autoCorrect={false}
              autoCapitalize='none' />
            <TextInput
              id="signup-form-contact-number"
              style={styles.signupFormInput}
              placeholder='Contact Number'
              value={signupFormData.contactNumber}
              onChangeText={handleFormInput}
              autoCorrect={false}
              autoCapitalize='none' />
            <TextInput
              id="signup-form-password"
              style={styles.signupFormInput}
              placeholder='Password'
              secureTextEntry
              value={signupFormData.password}
              onChangeText={handleFormInput}
              autoCorrect={false}
              autoCapitalize='none' />
            <TextInput
              id="signup-form-confirm-password"
              style={styles.signupFormInput}
              placeholder='Confirm Password'
              secureTextEntry
              value={signupFormData.confirmPassword}
              onChangeText={handleFormInput}
              autoCorrect={false}
              autoCapitalize='none' />
          </View>
          <View style={styles.signupFormBtnsContainer}>
            <View style={styles.signupFormBtnSignup}>
              <Pressable onPress={() => handleSignup()}><Text>Signup</Text></Pressable>
            </View>
            <Text style={styles.signupFormBtnLoginSection}>
              Have an Account?
              <Pressable onPress={() => changeFormState(FormState.LOGIN)}>
                <Text style={styles.signupFormBtnLogin}> <u>Login</u></Text>
              </Pressable></Text>
          </View>
        </SafeAreaView>
      </View>
    </View>
  )
}


const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: colorPallet.celadon,
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  modalHeaderView: {
    display: 'flex',
    flexDirection: 'row',
  },
  modelCancelBtnView: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },
  closeButton: {},
  modalCloseTextStyle: {},
  signupFormContainer: {
    display: 'flex',
    flexDirection: 'column'

  },
  signupFormHeaderText: {
    alignSelf: 'center'
  },
  signupFormInputView: {
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'center',
  },
  signupFormInput: {
    borderStyle: 'solid',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 10,
    height: 35,
    padding: 10,
    margin: 8

  },
  signupFormBtnsContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    margin: 8
  },
  signupFormBtnSignup: {
    backgroundColor: colorPallet.cambridgeBlue,
    borderRadius: 10,
    height: 30,
    width: 70,
    padding: 6,
    margin: 3
  },
  signupFormBtnLoginSection: {

  },
  signupFormBtnLogin: {
    color: colorPallet.caribbeanCurrent
  }

})