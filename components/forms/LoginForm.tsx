import React, { useEffect, useState } from 'react'
import { Alert, Button, Image, Pressable, SafeAreaView, StyleSheet, Switch, Text, TextInput, View } from 'react-native'
import { LoginService } from '../../services/LoginService';
import { colorPallet } from '../../constants/Colors';
import { FormState } from '../enums';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { setUserState } from "../../store/userSlice";
import { useLoginMutation } from '../../store/endpoints';
import { store } from '../../store/store';

export default function LoginForm(props: any) {
  const { setModalVisible, formState, setFormState } = props;
  const [login, result] = useLoginMutation();  
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  
  let userDetail = useAppSelector(state => state.user);
  const dispatch = useAppDispatch();

  const changeFormState = (state: FormState) => {
    if (formState != state) {
      setFormState(state)
    }
  }
  const handleForgotPassword = () => {

  }

  useEffect(() => {
    if (userDetail.access_token != "null") {
      console.log(userDetail);
    }
  }, [userDetail]);


  const handleLogin = async () => {
    const payload = { email, password }
    try {
      const response = await login(payload);
      if (response.data) dispatch(setUserState(response.data))
    } catch(ex: any){
      console.error("Error: ", ex);
    }
  }

  return (
    <View style={styles.centeredView}>
      <View style={styles.modalView}>
        <View style={styles.modalHeaderView}>
          <Text style={styles.loginFormHeaderText}>Login</Text>
        </View>
        <SafeAreaView style={styles.loginFormContainer}>
          <View style={styles.loginFormInputView}>
            <TextInput
              style={styles.loginFormInput}
              placeholder='email'
              value={email}
              onChangeText={setEmail}
              autoCorrect={false}
              autoCapitalize='none' />
            <TextInput
              style={styles.loginFormInput}
              placeholder='password'
              secureTextEntry
              value={password}
              onChangeText={setPassword}
              autoCorrect={false}
              autoCapitalize='none' />
          </View>
          <View style={styles.loginFormBtnsContainer}>
            <View style={styles.loginFormBtnForgotPassSection}>
              <Pressable onPress={handleForgotPassword}>
                <Text>Forgot Password?</Text>
              </Pressable>
            </View>
            <View style={styles.loginFormBtnLoginSection}>
              <Pressable onPress={() => handleLogin()}><Text>Login</Text></Pressable>
            </View>
            <Text style={styles.loginFormBtnSignupSection}>
              Don't Have Account?
              <Pressable style={styles.loginFormBtnSignup} onPress={() => changeFormState(FormState.SIGNUP)}>
                <Text> Sign Up</Text>
              </Pressable></Text>
          </View>
        </SafeAreaView>
      </View>
    </View>
  )
}


const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "#cbcfa1",
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  modalHeaderView: {
    display: 'flex',
    flexDirection: 'row',
  },
  modelCancelBtnView: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },
  closeButton: {},
  modalCloseTextStyle: {},
  loginFormContainer: {
    display: 'flex',
    flexDirection: 'column'

  },
  loginFormHeaderText: {
    alignSelf: 'center'
  },
  loginFormInputView: {
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'center',
  },
  loginFormInput: {
    borderStyle: 'solid',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 10,
    height: 35,
    padding: 10,
    margin: 8

  },
  loginFormBtnsContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'

  },
  loginFormBtnForgotPassSection: {
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'center',
    padding: 6
  },
  loginFormBtnLoginSection: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#a2a212",
    borderRadius: 10,
    height: 30,
    width: 70,
    margin: 3
  },
  loginFormBtnSignupSection: {

  },
  loginFormBtnSignup: {

  }

})