import { RestClient } from "./rest_client";
import { ILoginPayload, ISignupPayload } from "./Interfaces";

export class LoginService {
  private static _inst: LoginService;
  private restClient: RestClient;

  private constructor() {
    this.restClient = RestClient.getInstance();
  }

  public static getInstance() {
    if (!LoginService._inst) {
      LoginService._inst = new LoginService()
    }
    return LoginService._inst;
  }

  async login(loginPayload: ILoginPayload) {
    return await this.restClient.post("/users/login", {
      data: loginPayload
    });
  }

  async signup(signupPayload: ISignupPayload) {
    return await this.restClient.post("/users", {
      data: signupPayload
    });
  }

}