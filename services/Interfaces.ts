export interface ILoginPayload {
  email: string;
  password: string;
}

export interface ISignupPayload {
  first_name: string;
  last_name: string;
  school_id: string;
  email: string;
  primary_contact_number: string;
  password: string;
  user_type: string;
}