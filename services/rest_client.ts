import axios from 'axios';

enum RequestMethodEnum {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  DELETE = 'DELETE',
  PATCH = 'PATCH'
}

interface RequestConfig {
  url: string;
  method: RequestMethodEnum;
  data: any;
  headers: any;
  params: any;
}

export class RestClient {
  static _inst: RestClient;
  baseUrl = '';
  axios: any;

  private constructor() {
    this.baseUrl = 'http://localhost:8000/api/v1';
    this.axios = axios.create({
      baseURL: this.baseUrl
    })
  }

  public static getInstance() {
    if (!RestClient._inst) {
      RestClient._inst = new RestClient();
    }
    return RestClient._inst;
  }

  get = async (uri: string, params: any) => {
    const config: any = {}
    if (params.data) config.data = params.data;
    if (params.header) config.header = params.header;
    const response = await this.axios.get(uri, config);
    return response.data;
  }

  post = async (uri: string, params: any) => {
    const config: any = {}
    if (params.header) config.header = params.header;
    const response = await this.axios.post(uri, params.data);
    return response.data;  
  }
}